# Maintainer: Philip Müller <philm[at]manjaro[dot]org>
# Maintainer: Bernhard Landauer <bernhard[at]manjaro[dot]org>
# Maintainer: Helmut Stult <helmut[at]manjaro[dot]org>

# Arch credits:
# Maintainer : Thomas Baechler <thomas@archlinux.org>

_linuxprefix=linux510
_extramodules=extramodules-5.10-MANJARO
# don't edit here
pkgver=430.64_5.10.82_1

_nver=430
# edit here for new version
_sver=64
# edit here for new build
pkgrel=1
pkgname=$_linuxprefix-nvidia-${_nver}xx
_pkgname=nvidia
_pkgver="${_nver}.${_sver}"
pkgdesc="NVIDIA drivers for linux."
arch=('x86_64')
url="http://www.nvidia.com/"
makedepends=("$_linuxprefix" "$_linuxprefix-headers" "nvidia-${_nver}xx-utils=${_pkgver}")
groups=("$_linuxprefix-extramodules")
provides=("${_pkgname}=${_pkgver}" "nvidia-${_nver}xx-modules=${_pkgver}")
conflicts=("nvidia-${_nver}xx-dkms" "$_linuxprefix-nvidia" "$_linuxprefix-nvidia-340xx" "$_linuxprefix-nvidia-390xx" "$_linuxprefix-nvidia-418xx"
           "$_linuxprefix-nvidia-435xx" "$_linuxprefix-nvidia-440xx" "$_linuxprefix-nvidia-450xx" "$_linuxprefix-nvidia-455xx"
           "$_linuxprefix-nvidia-460xx")
license=('custom')
install=nvidia.install
options=(!strip)
durl="http://us.download.nvidia.com/XFree86/Linux-x86"
source=("${durl}_64/${_pkgver}/NVIDIA-Linux-x86_64-${_pkgver}-no-compat32.run"
        "license.patch" "kernel-5.5.patch" "kernel-5.6.patch" "kernel-5.7.patch"
        "kernel-5.8.patch" "kernel-5.9.patch" "kernel-5.10.patch")
sha256sums=('b00e5f7eda72e2b0f02b374138097757538bfeb75f79b1cdae4c681bd00700cd'
            '0421582b83f0c161102bf7fd48a30f0f0809a12ba382fccb752bfb3b7d3e3549'
            '85411fb607b97b6f3dcf0399a208618cfdf6f69b244d27e981a973fc93917c32'
            '25e4ad8f2d216637a51800f559c2bac49eb92c2112e7c21049a2b4234b68ba92'
            '82dbe342b0cff9c44301539957e00347a9b0d41d7aecedc93b6a55d156f7a29a'
            'f443afbac9f4aabfa2c0d1443b136bf42d9a17677d27c8860c806e5b4c99d4d8'
            '3f0d8f7724cd035cf51033d667a3fb0dbd57b2488af4ead4dbda21a91545ef61'
            '28385c854cb1b5950961f33d8980fbbdba9aacb903eb68b07930c8920da63302')

_pkg="NVIDIA-Linux-x86_64-${_pkgver}-no-compat32"

pkgver() {
    _ver=$(pacman -Q $_linuxprefix | cut -d " " -f 2)
    printf '%s' "${_pkgver}_${_ver/-/_}"
}

prepare() {
    sh "${_pkg}.run" --extract-only
    cd "${_pkg}"
    # patches here
    # original patches https://www.if-not-true-then-false.com/2020/inttf-nvidia-patcher
    # Fix compile problem with license
    msg2 "PATCH: license"
    patch -p1 --no-backup-if-mismatch -i "$srcdir"/license.patch

    # Fix compile problem with 5.5
    msg2 "PATCH: kernel-5.5"
    patch -p1 --no-backup-if-mismatch -i "$srcdir"/kernel-5.5.patch

    # Fix compile problem with 5.6
    msg2 "PATCH: kernel-5.6"
    patch -p1 --no-backup-if-mismatch -i "$srcdir"/kernel-5.6.patch

    # Fix compile problem with 5.7
    msg2 "PATCH: kernel-5.7"
    patch -p1 --no-backup-if-mismatch -i "$srcdir"/kernel-5.7.patch

    # Fix compile problem with 5.8
    msg2 "PATCH: kernel-5.8"
    patch -p1 --no-backup-if-mismatch -i "$srcdir"/kernel-5.8.patch

    # Fix compile problem with 5.9
    msg2 "PATCH: kernel-5.9"
    patch -p1 --no-backup-if-mismatch -i "$srcdir"/kernel-5.9.patch

    # Fix compile problem with 5.10
    msg2 "PATCH: kernel-5.10"
    patch -p1 --no-backup-if-mismatch -i "$srcdir"/kernel-5.10.patch
}

build() {
    _kernver="$(cat /usr/lib/modules/${_extramodules}/version)"
    cd "${_pkg}"/kernel
    make SYSSRC=/usr/lib/modules/"${_kernver}/build" module
}

package() {
    _ver=$(pacman -Q $_linuxprefix | cut -d " " -f 2)
    depends=("${_linuxprefix}=${_ver}")

    install -D -m644 "${srcdir}/${_pkg}/kernel/nvidia.ko" \
        "${pkgdir}/usr/lib/modules/${_extramodules}/nvidia.ko"
    install -D -m644 "${srcdir}/${_pkg}/kernel/nvidia-modeset.ko" \
         "${pkgdir}/usr/lib/modules/${_extramodules}/nvidia-modeset.ko"
    install -D -m644 "${srcdir}/${_pkg}/kernel/nvidia-drm.ko" \
         "${pkgdir}/usr/lib/modules/${_extramodules}/nvidia-drm.ko"
    install -D -m644 "${srcdir}/${_pkg}/kernel/nvidia-uvm.ko" \
         "${pkgdir}/usr/lib/modules/${_extramodules}/nvidia-uvm.ko"
    gzip "${pkgdir}/usr/lib/modules/${_extramodules}/"*.ko
    sed -i -e "s/EXTRAMODULES='.*'/EXTRAMODULES='${_extramodules}'/" "${startdir}/nvidia.install"
}
